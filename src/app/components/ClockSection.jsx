'use client';
import React, { useEffect, useRef } from 'react';

const Clock = () => {
	const angularGradientRef = useRef(null);

	useEffect(() => {
		const resizeHandler = () => {
			const { current: angularGradientElement } = angularGradientRef;
			if (angularGradientElement) {
				const { innerWidth, innerHeight } = window;
				const size = Math.max(innerWidth, innerHeight) + 300;
				const left = (innerWidth - size) / 2;
				const top = (innerHeight - size) / 2;
				angularGradientElement.style.width = `${size}px`;
				angularGradientElement.style.height = `${size}px`;
				angularGradientElement.style.left = `${left}px`;
				angularGradientElement.style.top = `${top}px`;
			}
		};

		resizeHandler();

		window.addEventListener('resize', resizeHandler);
		return () => {
			window.removeEventListener('resize', resizeHandler);
		};
	}, []);

	const angularGradientStyle = {
		position: 'fixed',
		top: 0,
		left: 0,
		width: '100vw',
		height: '100vh',
		background: 'conic-gradient(from 0deg, #003333 0%, #e6ffff 100%)',
		animation: 'rotation 10s linear infinite',
		overflow: 'hidden',
		transformOrigin: 'center',
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center',
		borderRadius: '50%',
	};

	return <div ref={angularGradientRef} style={angularGradientStyle}></div>;
};

export default Clock;
