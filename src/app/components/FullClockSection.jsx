import React from 'react';
import Clock from './ClockSection';
import FullScreenSection from './FullScreenSection';
import TimeGoalsSection from './TimeGoalsSection';

const FullClockSection = () => {
	return (
		<FullScreenSection justifyContent="center" alignItems="center">
			<Clock />
			<div className="grid grid-cols-1 sm:grid-cols-12 grid-rows-2 z-10 relative">
				<div className="col-span-3 place-self-center">
					<h1 className="text-[#055d56] mb-4 text-xl lg:text-6xl sm:text-5xl text-center sm:text-left font-extrabold">
						The Clock <br />
						Is <span className="text-white">Ticking</span>
					</h1>
					<p className="text-[#121212] text-base sm:text-lg lg:text-xl mb-6 mr-3">
						Time is a valuable resource. Make the most of every moment!
					</p>
				</div>
				<span className="col-span-4 row-span-1"></span>
				<TimeGoalsSection className="col-span-12 row-span-2 sm:col-span-12" />
			</div>
		</FullScreenSection>
	);
};

export default FullClockSection;
