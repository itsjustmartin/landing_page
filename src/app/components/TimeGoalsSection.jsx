'use client';
import React from 'react';
import dynamic from 'next/dynamic';

const AnimatedNumbers = dynamic(
	() => {
		return import('react-animated-numbers');
	},
	{ ssr: false }
);

const TimeGoalsSection = () => {
	const timeGoalsList = [
		{
			metric: 'Projects',
			value: '100',
			postfix: '+',
		},
		{
			prefix: '~',
			metric: 'Users',
			value: '100,000',
		},
		{
			metric: 'Awards',
			value: '7',
		},
		{
			metric: 'Years',
			value: '5',
		},
	];
	return (
		<div className="py-4 px-2 xl:gap-16 sm:py-16 xl:px-8">
			<div className="sm:border-[#055d56] sm:border rounded-md py-4 px-8 flex flex-col sm:flex-row items-center justify-between">
				{timeGoalsList.map((achievement, index) => {
					return (
						<div
							key={index}
							className="flex flex-col items-center justify-center mx-4 my-4 sm:my-0"
						>
							<h2 className="text-[#055d56] text-4xl font-bold flex flex-row">
								{achievement.prefix}
								<AnimatedNumbers
									includeComma
									animateToNumber={parseInt(achievement.value)}
									locale="en-US"
									className="text-[#055d56]  text-4xl font-bold"
									configs={(_, index) => {
										return {
											mass: 1,
											friction: 100,
											tensions: 140 * (index + 1),
										};
									}}
								/>
								{achievement.postfix}
							</h2>
							<p className="text-[#055d56] text-base">{achievement.metric}</p>
						</div>
					);
				})}
			</div>
		</div>
	);
};

export default TimeGoalsSection;
