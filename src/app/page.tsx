import { ChakraProvider } from '@chakra-ui/react';
import FullClockSection from './components/FullClockSection.jsx';

export default function Home() {
	return (
		<main className="flex min-h-screen flex-col justify-between  bg-[#c7c3c3]">
			<FullClockSection />
		</main>
	);
}
